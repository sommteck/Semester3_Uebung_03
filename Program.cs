    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung_3
{
    class CPerson
    {
        protected string name;
        public CPerson()
        {
            name = "";
        }

        public CPerson(string nameParameter)
        {
            name = nameParameter;
        }

        public void Ausgeben()
        {
            Console.WriteLine("Name der Person ist " + name);
        }
    }

    class CKunde : CPerson
    {
        private int KundenID;
        public CKunde() : base()
        {
            KundenID = 0;
        }

        public CKunde(string nameParameter, int KundenIDParameter) : base (nameParameter)
        {
            KundenID = KundenIDParameter;
        }

        public void Ausgabe()
        {
            Console.WriteLine("Name des Kunden: " + name);
            Console.WriteLine("ID des Kunden: " + KundenID);
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            CKunde ersterKunde = new CKunde("Huber", 456);
            CKunde zweiterKunde = new CKunde("Mauer", 123);
            CPerson dritterKunde = new CPerson("Müller");
            ersterKunde.Ausgabe();
            zweiterKunde.Ausgabe();
            dritterKunde.Ausgeben();
            ersterKunde.Ausgeben();
            Console.ReadKey();
        }
    }
}
